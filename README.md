![logo.png](ressources/imgs/logo.png)

## Description

Jeu de la bataille du café dans le cadre du Projet Tutoré du Semestre 3.

## Diagramme UML

![uml.png](ressources/uml.png)

## Tests Unitaires

Les tests unitaires sont implémentés.

## Auteurs

- [Yacine Schamel](https://gitlab.com/Schamlee)

- [Nicolas Davenne](https://gitlab.com/nico.dvne)

- [Hugo Gennevée](https://gitlab.com/HugoGennevee)

- [Maxime Petit](https://gitlab.com/PetitM)