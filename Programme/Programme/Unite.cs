﻿using System;
using System.Linq;

namespace Programme
{
    public class Unite
    {

        #region Attributs

        public const int defaultParcelleValue = 17;

        private int numTrame;
        private char type;
        private bool frontiereNord;
        private bool frontiereEst;
        private bool frontiereSud;
        private bool frontiereOuest;

        private int parcelle;
        private int abscisse;
        private int ordonnee;

        private static char[] listeTypes = { 'a', 'F', 'M', 'S', 'C', 'Z' };

        #endregion

        #region Propriétés

        public int NumTrame
        {
            get { return numTrame; }
            set { numTrame = value; }
        }

        public char Type
        {
            get { return type; }
            set
            {
                if (!listeTypes.Contains(value))
                    throw new ArgumentException("Le type attribué à la parcelle est incorrect !");

                type = value;
            }
        }

        public bool FrontiereNord
        {
            get { return frontiereNord; }
            set { frontiereNord = value; }
        }

        public bool FrontiereEst
        {
            get { return frontiereEst; }
            set {
                frontiereEst = value; }
        }

        public bool FrontiereSud
        {
            get { return frontiereSud; }
            set { frontiereSud = value; }
        }

        public bool FrontiereOuest
        {
            get { return frontiereOuest; }
            set { frontiereOuest = value; }
        }

        public int Parcelle
        {
            get { return parcelle; }
            set
            {
                if (value < 0 || value > defaultParcelleValue + 1)
                    throw new ArgumentOutOfRangeException($"{nameof(value)} ne peut être < 0 ou > 17. ");

                parcelle = value;
            }
        }

        public int Abscisse
        {
            get { return this.abscisse; }
            set
            {
                if (value < 0 || value > 9) // La carte est composée de 10 lignes et 10 colonnes
                    throw new ArgumentOutOfRangeException($"{nameof(value)} ne peut pas être < 0 ou > 9 ");

                abscisse = value;
            }
        }

        public int Ordonnee
        {
            get { return this.ordonnee; }
            set
            {
                if (value < 0 || value > 9) // La carte est composée de 10 lignes et 10 colonnes
                    throw new ArgumentOutOfRangeException($"{nameof(value)} ne peut pas être < 0 ou > 9 ");

                ordonnee = value;
            }
        }

        #endregion

        #region Constructeur

        public Unite(int valeurCase)
        {
            int positionFrontieres = initType(valeurCase);
            initFrontieres(positionFrontieres);
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// La méthode évalue et attribue le type de la case
        /// Types de case :
        /// - 'M' : mer
        /// - 'F' : forêt
        /// - 'a' : case jouable
        /// </summary>
        /// <param name="valeurCase">Est la valeur de la case fournie dans la trame</param>
        /// <returns>Un nombre correspondant à la position des frontières avec ses voisins</returns>
        private int initType(int valeurCase)
        {
            NumTrame = valeurCase; // On enregistre en mémoire le numéro de la case fourni dans la trame

            Type = 'a';

            if (valeurCase - 64 > 0)
            {
                Type = 'M';
                valeurCase -= 64;
            }
            else if (valeurCase - 32 > 0)
            {
                Type = 'F';
                valeurCase -= 32;
            }

            return valeurCase;
        }

        /// <summary>
        /// La méthode évalue et attribue la position des frontières par rapport au numéro de la case
        /// </summary>
        /// <param name="positionFrontieres">Valeur de la case, donnant la position des frontières</param>
        private void initFrontieres(int positionFrontieres)
        {
            /* Frontière Est */
            if (positionFrontieres != 0)
            {
                positionFrontieres -= 8;
                FrontiereEst = true;
            }
            if (positionFrontieres < 0)
            {
                positionFrontieres += 8;
                FrontiereEst = false;
            }

            /* Frontière Sud */
            if (positionFrontieres != 0)
            {
                positionFrontieres -= 4;
                FrontiereSud = true;
            }
            if (positionFrontieres < 0)
            {
                positionFrontieres += 4;
                FrontiereSud = false;
            }

            /* Frontière Ouest */
            if (positionFrontieres != 0)
            {
                positionFrontieres -= 2;
                FrontiereOuest = true;
            }
            if (positionFrontieres < 0)
            {
                positionFrontieres += 2;
                FrontiereOuest = false;
            }

            /* Frontière Nord */
            if (positionFrontieres != 0)
            {
                positionFrontieres -= 1;
                FrontiereNord = true;
            }
            if (positionFrontieres < 0)
            {
                positionFrontieres += 1;
                FrontiereNord = false;
            }

        }

        /// <summary>
        /// La méthode évalue si la case est jouable par l'utilisateur
        /// </summary>
        /// <returns>True si la case est jouable, false sinon</returns>
        public bool isJouable()
        {
            if (Type == 'a')
                return true;
            else return false;

        }

        #endregion

    }
}