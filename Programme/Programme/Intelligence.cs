﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Programme
{
    public class Intelligence
    {
        #region Constantes 

        public static int NB_GRAINES_DEBUT_PARTIE = 28;
        public static int TAILLE_PLUS_GRANDE_PARCELLE = 6;

        #endregion

        #region Attributs

        Carte m_carteDeJeu;
        Unite[,] m_carte;

        Unite m_derniereUnitePlantee;
        Unite m_derniereUniteAdversaire;

        Socket socket;

        int m_nbGrainesRestantes;

        #endregion

        #region Propriétés

        public int NbGrainesRestantes
        {
            get { return m_nbGrainesRestantes; }
            private set { m_nbGrainesRestantes = value; }
        }

        public Unite DerniereUnitePlantee
        {
            get { return m_derniereUnitePlantee; }
            private set { m_derniereUnitePlantee = value; }
        }

        public Unite DerniereUniteAdversaire
        {
            get { return m_derniereUniteAdversaire; }
            private set { m_derniereUniteAdversaire = value; }
        }

        public Carte CarteDeJeu
        {
            get { return m_carteDeJeu; }
            private set { m_carteDeJeu = value; }
        }

        public Unite[,] Carte2Dim
        {
            get { return m_carte; }
            private set { m_carte = value; }
        }

        #endregion

        #region Constructeur

        /// <summary>
        /// Constructeur par défaut et unique 
        /// L'IA prend connaissance de la carte de jeu et initialise ses attributs
        /// </summary>
        /// <param name="p_carte">Carte de jeu</param>
        public Intelligence(Carte p_carte, Socket p_socket)
        {
            CarteDeJeu = p_carte;
            m_carte = CarteDeJeu.CarteJeu;

            NbGrainesRestantes = Intelligence.NB_GRAINES_DEBUT_PARTIE;

            DerniereUniteAdversaire = null;
            DerniereUnitePlantee = null;

            socket = p_socket;
        }

        #endregion

        #region Méthodes 

        #region Mise à jour des données de la partie (à chaque tour)

        /// <summary>
        /// Décrémente le nombre de graines de l'IA à chaque tour
        /// </summary>
        private void majGrainesRestantes()
        {
            NbGrainesRestantes = NbGrainesRestantes - 1;
            Console.WriteLine("Il ne reste plus que {0} graines à l'IA !", NbGrainesRestantes);
        }

        /// <summary>
        /// Met à jour l'Unite jouée par l'adversaire en changeant son type en 'S'
        /// </summary>
        private void majUniteAdversaire()
        {
            DerniereUniteAdversaire.Type = 'S';
        }

        /// <summary>
        /// Met à jour l'Unite jouée par l'IA en changeant son type en 'C'
        /// </summary>
        private void majUniteIA()
        {
            DerniereUnitePlantee.Type = 'C';
        }

        #endregion

        #region R1 : La première pose est libre (Validé)

        /// <summary>
        /// La méthode permet de déterminer la ligne qui comporte le plus de Parcelles
        /// </summary>
        /// <returns>Le numéro de ligne avec le nombre de Parcelles le plus grand sur la Carte</returns>
        private int rechercheLigneOptimale()
        {
            List<int> listeNumParcellesLigne = new List<int>();
            int nbParcellesLigne = 0;
            int ligneRetenue = 0;
            int nbParcellesMax = 0;

            for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
            {
                for (int numColonne = 0; numColonne < Carte.nbCol; numColonne++)
                {
                    if (m_carte[numLigne, numColonne].isJouable() && !listeNumParcellesLigne.Contains(m_carte[numLigne, numColonne].Parcelle))
                    {
                        nbParcellesLigne++;
                        listeNumParcellesLigne.Add(m_carte[numLigne, numColonne].Parcelle);
                    }
                }

                if (nbParcellesLigne > nbParcellesMax)
                {
                    ligneRetenue = numLigne;
                    nbParcellesMax = nbParcellesLigne;
                }

                nbParcellesLigne = 0;
                listeNumParcellesLigne.Clear();
            }

            return ligneRetenue;
        }

        /// <summary>
        /// La méthode permet de déterminer la colonne qui comporte le plus de Parcelles
        /// </summary>
        /// <returns>Le numéro de colonne avec le nombre de Parcelles le plus grand</returns>
        private int rechercheColonneOptimale()
        {
            List<int> listeNumParcellesColonne = new List<int>();
            int nbParcellesColonne = 0;
            int colonneRetenue = 0;
            int nbParcellesMax = 0;

            for (int numCol = 0; numCol < Carte.nbCol; numCol++)
            {
                for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
                {
                    if (m_carte[numLigne, numCol].isJouable() && !listeNumParcellesColonne.Contains(m_carte[numLigne, numCol].Parcelle))
                    {
                        nbParcellesColonne++;
                        listeNumParcellesColonne.Add(m_carte[numLigne, numCol].Parcelle);
                    }
                }

                if (nbParcellesColonne > nbParcellesMax)
                {
                    colonneRetenue = numCol;
                    nbParcellesMax = nbParcellesColonne;
                }

                nbParcellesColonne = 0;
                listeNumParcellesColonne.Clear();
            }

            return colonneRetenue;
        }

        /// <summary>
        /// La méthode évalue si la case élue pour être jouée en premier est jouable
        /// </summary>
        /// <returns>Evalue si la première case choisie est jouable</returns>
        private bool isPremiereCaseJouable()
        {
            if (m_carte[rechercheLigneOptimale(), rechercheColonneOptimale()].isJouable())
            {
                // Console.WriteLine("La première case est jouable !");
                return true;
            }


            return false;
        }

        /// <summary>
        /// La méthode calcule le nombre de colonnes jouables sur une ligne de la carte
        /// </summary>
        /// <param name="ligne">Est la ligne que l'on évalue</param>
        /// <returns>Le nombre de colonnes jouables sur une ligne de la carte</returns>
        private int getNbColonnesJouablesSurLigne(int ligne)
        {
            int nbColonnesJouables = 0;

            for (int numCol = 0; numCol < Carte.nbCol; numCol++)
                if (m_carte[ligne, numCol].isJouable())
                    nbColonnesJouables = nbColonnesJouables + 1;

            return nbColonnesJouables;
        }

        /// <summary>
        /// La méthode calcule le nombre de lignes jouables sur une colonne de la carte
        /// </summary>
        /// <param name="colonne">Est la colonne que l'on évalue</param>
        /// <returns>Le nombre de lignes jouables sur une colonne de la carte</returns>
        private int getNbLignesJouablesSurColonne(int colonne)
        {
            int nbLignesJouables = 0;

            for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
                if (m_carte[numLigne, colonne].isJouable())
                    nbLignesJouables = nbLignesJouables + 1;

            return nbLignesJouables;
        }

        /// <summary>
        /// La méthode retourne le nombre de possibilités de coups suivants à partir d'une position sur la carte
        /// La somme des unités jouables sur la ligne et sur la colonne
        /// </summary>
        /// <param name="ligne">Est la ligne que l'on évalue</param>
        /// <param name="colonne">Est la colonne que l'on évalue</param>
        /// <returns>La somme des unités éventuellement jouables au second tour</returns>
        private int getNbPossibilitesDeCoupSuivant(int ligne, int colonne)
        {
            return getNbColonnesJouablesSurLigne(ligne) + getNbLignesJouablesSurColonne(colonne);
        }

        /// <summary>
        /// Dans l'idéal, il faut jouer le coup qui ouvre la voix à la plus grande ligne et plus grande colonne de la carte
        /// Cependant, si l'intersection entre cette ligne et cette colonne n'est pas une unité jouable, il faut trouver une seconde possibilité
        /// On cherche alors une unité jouable qui ouvre la voix au plus grand nombre de coups suivants possibles
        /// </summary>
        /// <returns></returns>
        private Unite jouerPremierCoupIntermediaire()
        {
            Unite uniteRetenue = null;
            int possibilitesCoupsMax = 0;

            for (int ligne = 0; ligne < Carte.nbLine; ligne++)
            {
                for (int col = 0; col < Carte.nbCol; col++)
                {
                    int nbCoups = 0;

                    if (m_carte[ligne, col].isJouable())
                        nbCoups = getNbPossibilitesDeCoupSuivant(ligne, col);

                    if (nbCoups > possibilitesCoupsMax)
                    {
                        possibilitesCoupsMax = nbCoups;
                        uniteRetenue = m_carte[ligne, col];
                    }
                }
            }

            return uniteRetenue;
        }

        /// <summary>
        /// L'IA choisit l'Unite qu'elle va jouer
        /// </summary>
        /// <returns>L'Unite choisie par l'IA pour le premier coup</returns>
        private Unite jouerPremierTour()
        {
            if (isPremiereCaseJouable())
                return m_carte[rechercheLigneOptimale(), rechercheColonneOptimale()];
            else
                return jouerPremierCoupIntermediaire();
        }

        #endregion

        #region R2 : Ne pas poser une graine sur une unité de type Mer ou Forêt

        // Avant chaque choix d'Unite, on s'assure que la case est jouable => Unite.isJouable()

        #endregion

        #region R3 et R4 : Poser une graîne sur la même ligne ou colonne que la dernière Unite plantée, mais pas sur la même Parcelle

        #region Compter les Unites Jouables

        private int getNbUnitePasDansParcelleEnnemiLigne(int ligneChoisie)
        {
            int nbUnites = 0;

            for (int numCol = 0; numCol < Carte.nbCol; numCol++)
                if (m_carte[ligneChoisie, numCol].isJouable() && m_carte[ligneChoisie, numCol].Parcelle != DerniereUniteAdversaire.Parcelle)
                    nbUnites++;

            return nbUnites;
        }

        private int getNbUnitePasDansParcelleEnnemiColonne(int colonneChoisie)
        {
            int nbUnite = 0;

            for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
                if (m_carte[numLigne, colonneChoisie].isJouable() && m_carte[numLigne, colonneChoisie].Parcelle != DerniereUniteAdversaire.Parcelle)
                    nbUnite++;

            return nbUnite;
        }

        #endregion

        #region Jouer une Unite Voisine si Possible

        /// <summary>
        /// Retourne le nombre d'unités jouables de la Parcelle Ouest, par rapport à la dernière unité jouée par notre IA
        /// </summary>
        /// <returns>Le nombre d'unités jouables de la Parcelle Ouest (si différente du numéro de parcelle de la dernière Unité jouée par l'adv)</returns>
        private int tailleParcelleOuest()
        {
            int nbUnitesParcelleOuest = 0;

            try
            {
                if (m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].isJouable())
                    if (m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].Parcelle != DerniereUniteAdversaire.Parcelle)
                    {
                        Parcelle[] parcelles = m_carteDeJeu.Parcelles;
                        int nbUnitesJouablesParcelle = parcelles[m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].Parcelle].nbUnitesJouables();
                        // Console.WriteLine("L'Unite Ouest [{0}, {1}] peut être choisie pour jouer ce tour ! Elle compte {2} Unites jouables ", DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1, nbUnitesJouablesParcelle);
                        return nbUnitesJouablesParcelle;

                        // Ne pas insérer cette condition avant vérification : if (!parcelles[m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].Parcelle].isControledByEnemy())
                    }

                return nbUnitesParcelleOuest;
            }
            catch
            {
                return nbUnitesParcelleOuest;
            }
        }

        /// <summary>
        /// Retourne le nombre d'unités jouables de la Parcelle Est, par rapport à la dernière unité jouée par notre IA
        /// </summary>
        /// <returns>Le nombre d'unités jouables de la Parcelle Est (si différente du numéro de parcelle de la dernière Unité jouée par l'adv)</returns>
        private int tailleParcelleEst()
        {
            int nbUnitesParcelleEst = 0;

            try
            {
                if (m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1].isJouable())
                    if (m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1].Parcelle != DerniereUniteAdversaire.Parcelle)
                    {
                        Parcelle[] parcelles = m_carteDeJeu.Parcelles;
                        int nbUnitesJouablesParcelle = parcelles[m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1].Parcelle].nbUnitesJouables();
                        // Console.WriteLine("L'Unite Est [{0}, {1}] peut être choisie pour jouer ce tour ! Elle compte {2} Unites jouables ", DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1, nbUnitesJouablesParcelle);
                        return nbUnitesJouablesParcelle;

                        // Ne pas insérer cette condition avant vérification : if (!parcelles[m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1].Parcelle].isControledByEnemy())
                    }

                return nbUnitesParcelleEst;
            }
            catch
            {
                return nbUnitesParcelleEst;
            }
        }

        /// <summary>
        /// Retourne le nombre d'unités jouables de la Parcelle Nord, par rapport à la dernière unité jouée par notre IA
        /// </summary>
        /// <returns>Le nombre d'unités jouables de la Parcelle Nord (si différente du numéro de parcelle de la dernière Unité jouée par l'adv)</returns>
        private int tailleParcelleNord()
        {
            int nbUnitesParcelleNord = 0;

            try
            {
                if (m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee].isJouable())
                    if (m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee].Parcelle != DerniereUniteAdversaire.Parcelle)
                    {
                        Parcelle[] parcelles = m_carteDeJeu.Parcelles;
                        int nbUnitesJouablesParcelle = parcelles[m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee].Parcelle].nbUnitesJouables();
                        // Console.WriteLine("L'Unite Est [{0}, {1}] peut être choisie pour jouer ce tour ! Elle compte {2} Unites jouables ", DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee, nbUnitesJouablesParcelle);
                        return nbUnitesJouablesParcelle;

                        // Ne pas insérer cette condition avant vérification : if (!parcelles[m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee].Parcelle].isControledByEnemy())
                    }

                return nbUnitesParcelleNord;
            }
            catch
            {
                return nbUnitesParcelleNord;
            }
        }

        /// <summary>
        /// Retourne le nombre d'unités jouables de la Parcelle Sud, par rapport à la dernière unité jouée par notre IA
        /// </summary>
        /// <returns>Le nombre d'unités jouables de la Parcelle Sud (si différente du numéro de parcelle de la dernière Unité jouée par l'adv)</returns>
        private int tailleParcelleSud()
        {
            int nbUnitesParcelleSud = 0;

            try
            {
                if (m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee].isJouable())
                    if (m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee].Parcelle != DerniereUniteAdversaire.Parcelle)
                    {
                        Parcelle[] parcelles = m_carteDeJeu.Parcelles;
                        int nbUnitesJouablesParcelle = parcelles[m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee].Parcelle].nbUnitesJouables();
                        // Console.WriteLine("L'Unite Est [{0}, {1}] peut être choisie pour jouer ce tour ! Elle compte {2} Unites jouables ", DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee, nbUnitesJouablesParcelle);
                        return nbUnitesJouablesParcelle;

                        // Ne pas insérer cette condition avant vérification : if (!parcelles[m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee].Parcelle].isControledByEnemy())
                    }

                return nbUnitesParcelleSud;
            }
            catch
            {
                return nbUnitesParcelleSud;
            }
        }

        /// <summary>
        /// Evalue si une unite voisine de la derniere Unite Plantee par l'IA est jouable
        /// </summary>
        /// <returns>True si une unité est jouable, False sinon</returns>
        private bool uneUniteEstJouableSurLaLigne()
        {
            if (DerniereUnitePlantee.Abscisse == DerniereUniteAdversaire.Abscisse)
                if ((tailleParcelleEst() > 0 || tailleParcelleOuest() > 0) && getNbUnitePasDansParcelleEnnemiLigne(DerniereUniteAdversaire.Abscisse) > 0)
                {
                    // Console.WriteLine("Une Unité est jouable sur la même Ligne que la dernière graine plantée !");
                    return true;
                }
                    
            return false;
        }

        /// <summary>
        /// Evalue si une unite voisine de la derniere Unite Plantee par l'IA est jouable sur la colonne
        /// </summary>
        /// <returns>True si une unité est jouable, False sinon</returns>
        private bool uneUniteEstJouableSurLaColonne()
        {
            if (DerniereUnitePlantee.Ordonnee == DerniereUniteAdversaire.Ordonnee)
                if ((tailleParcelleNord() > 0 || tailleParcelleSud() > 0) && getNbUnitePasDansParcelleEnnemiColonne(DerniereUniteAdversaire.Ordonnee) > 0)
                {
                    // Console.WriteLine("Une Unité est jouable sur la même Colonne que la dernière graine plantée !");
                    return true;
                }
                    

            return false;
        }

        /// <summary>
        /// Evalue si au moins l'une des parcelles voisines de la dernière unité plantée par notre IA est jouable
        /// </summary>
        /// <returns>True si c'est le cas, false sinon</returns>
        private bool isUneUniteVoisineJouable()
        {
            if (uneUniteEstJouableSurLaLigne())
                return uneUniteEstJouableSurLaLigne();

            if (uneUniteEstJouableSurLaColonne())
                return uneUniteEstJouableSurLaColonne();

            Console.WriteLine("Aucune unité voisine de la dernière Unite posée n'est jouable !");

            return false;
        }

        /// <summary>
        /// En fonction de la dernière pose de l'adversaire
        /// On va s'assurer que l'on peut jouer une Unite voisine de notre dernière Unite plantée (N, S, E, O)
        /// </summary>
        /// <returns>Si une Unite peut être jouée, on la renvoie. Sinon, la méthode renvoie null</returns>
        private Unite jouerUniteVoisine()
        {
            Unite uniteRetenue = null;

            if (DerniereUnitePlantee.Abscisse == DerniereUniteAdversaire.Abscisse && uneUniteEstJouableSurLaLigne())
            {
                if (tailleParcelleOuest() != 0 && tailleParcelleOuest() > tailleParcelleEst() && m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].isJouable())
                    return uniteRetenue = m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1];
                else if (tailleParcelleEst() != 0 && m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee - 1].isJouable())
                    return uniteRetenue = m_carte[DerniereUnitePlantee.Abscisse, DerniereUnitePlantee.Ordonnee + 1];
            }

            if (DerniereUnitePlantee.Ordonnee == m_derniereUniteAdversaire.Ordonnee && uneUniteEstJouableSurLaColonne())
            {
                if (tailleParcelleNord() != 0 && tailleParcelleNord() >= tailleParcelleSud() && m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee].isJouable())
                    return uniteRetenue = m_carte[DerniereUnitePlantee.Abscisse - 1, DerniereUnitePlantee.Ordonnee];
                else if (tailleParcelleSud() != 0 && m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee].isJouable())
                    return uniteRetenue = m_carte[DerniereUnitePlantee.Abscisse + 1, DerniereUnitePlantee.Ordonnee];
            }

            return uniteRetenue;
        }

        #endregion

        #region Jouer un Coup selon la stratégie de l'IA

        /// <summary>
        /// La méthode analyse chacune des lignes de la Map en choisissant comme critère la ligne qui compte le plus grand nombre de parcelles
        /// Le numéro de ligne dépendra de la dernière Unité jouée par l'adversaire
        /// </summary>
        /// <returns>Un tableau d'entiers contenant le numéro de la ligne retenue et le nombre de parcelles sur la ligne</returns>
        private int[] getMeilleureLigneJouable()
        {
            int ligneRetenue = 0;
            // int nbUniteMax = 0; // Ce paramètre peut être pris en compte dans une seconde stratégie. On s'intéresse ici au nombre maximum de parcelles sur une ligne.
            int nbMaxParcelleLigne = 0;
            List<int> listesParcelle = new List<int>();

            for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
            {
                int nbParcellesRencontrees = 0;
                if (m_carte[numLigne, DerniereUniteAdversaire.Ordonnee].isJouable() && m_carte[numLigne, DerniereUniteAdversaire.Ordonnee].Parcelle != DerniereUniteAdversaire.Parcelle)
                {
                    for (int col = 0; col < Carte.nbCol; col++)
                    {
                        if (m_carte[numLigne, col].isJouable() && !listesParcelle.Contains(m_carte[numLigne, col].Parcelle))
                        {
                            listesParcelle.Add(m_carte[numLigne, col].Parcelle);
                            nbParcellesRencontrees++;
                        }
                    }

                    if (nbParcellesRencontrees > nbMaxParcelleLigne)
                    {
                        ligneRetenue = numLigne;
                        nbMaxParcelleLigne = nbParcellesRencontrees;
                    }
                }

                listesParcelle.Clear();
            }

            int[] resultat = new int[] { ligneRetenue, nbMaxParcelleLigne };
            return resultat;
        }

        /// <summary>
        /// La méthode analyse chacune des colonnes de la Map en choisissant comme critère la colonnequi compte le plus grand nombre de parcelles
        /// Le numéro de colonne retourné dépendra de la dernière Uniteparcelle jouée par l'adversaire
        /// </summary>
        /// <returns>Un tableau d'entiers contenant le numéro de la colonne retenue et le nombre de parcelles sur la colonne</returns>
        private int[] getMeilleureColonneJouable()
        {
            int colRetenue = 0;
            // int nbUniteMax = 0; // Ce paramètre peut être pris en compte dans une seconde stratégie. On s'intéresse ici au nombre maximum de parcelles sur une ligne.
            int nbMaxParcelleColonne = 0;
            List<int> listesParcelle = new List<int>();

            for (int numCol = 0; numCol < Carte.nbCol; numCol++)
            {
                int nbParcellesRencontrees = 0;

                if (m_carte[DerniereUniteAdversaire.Abscisse, numCol].isJouable() && m_carte[DerniereUniteAdversaire.Abscisse, numCol].Parcelle != DerniereUniteAdversaire.Parcelle)
                {
                    for (int numLigne = 0; numLigne < Carte.nbLine; numLigne++)
                    {
                        if (m_carte[numLigne, numCol].isJouable() && !listesParcelle.Contains(m_carte[numLigne, numCol].Parcelle))
                        {
                            listesParcelle.Add(m_carte[numLigne, numCol].Parcelle);
                            nbParcellesRencontrees++;
                        }
                    }

                    if (nbParcellesRencontrees > nbMaxParcelleColonne)
                    {
                        colRetenue = numCol;
                        nbMaxParcelleColonne = nbParcellesRencontrees;
                    }

                }
                listesParcelle.Clear();
            }

            int[] resultat = new int[] { colRetenue, nbMaxParcelleColonne };
            return resultat;
        }

        /// <summary>
        /// L'IA choisit une Unite à jouer qui se trouve soit sur la même ligne, soit sur la même colonne que la dernière graîne plantée
        /// </summary>
        /// <returns>L'Unite demandée par l'IA</returns>
        private Unite jouerUniteNonVoisine()
        {
            Unite uniteJouee = null;

            int[] resultatLigne = getMeilleureLigneJouable();
            int[] resultatColonne = getMeilleureColonneJouable();

            if (resultatLigne[1] == 0)
                Console.WriteLine("On ne peut pas jouer ce coup sur la dernière ligne !");

            if (resultatColonne[1] == 0)
                Console.WriteLine("On ne peut pas jouer ce coup sur la dernière colonne !");

            if (resultatColonne[1] != 0 && resultatColonne[1] >= resultatLigne[1] && m_carte[DerniereUniteAdversaire.Abscisse, resultatColonne[0]].isJouable()) // Unite.isJouable()
                return m_carte[DerniereUniteAdversaire.Abscisse, resultatColonne[0]];
            else if (resultatLigne[1] != 0 && resultatLigne[1] > resultatColonne[1] /*&& m_carte[resultatLigne[1], DerniereUniteAdversaire.Ordonnee].isJouable()*/)
                return m_carte[resultatLigne[0], DerniereUniteAdversaire.Ordonnee];

            return uniteJouee;
        }

        #endregion

        #endregion

        #region Communiquer avec le serveur via un Socket

        /// <summary>
        /// La méthode communique avec le serveur et envoie l'Unite que l'IA a choisi de jouer
        /// </summary>
        /// <returns>Les coordonnées de l'Unité jouée par l'IA</returns>
        private int[] envoyerUniteChoisie()
        {
            // Console.WriteLine("L'IA joue son tour !");

            string reponseIA = null;

            // Le serveur attend une réponde du type A:XY ou X et Y sont les coordonnées de l'Unite que l'IA choisit

            if (NbGrainesRestantes == NB_GRAINES_DEBUT_PARTIE)
            {
                Console.WriteLine("Je joue le premier tour !");

                reponseIA = "A:" + jouerPremierTour().Abscisse + jouerPremierTour().Ordonnee;
                Console.WriteLine(reponseIA);
            }
            else
            {
                reponseIA = "A:" + jouerUnite().Abscisse + jouerUnite().Ordonnee;
            }

            byte[] byteToSend = Encoding.ASCII.GetBytes(reponseIA);
            socket.Send(byteToSend);

            char[] reponseEnvoyee = reponseIA.ToCharArray();

            majGrainesRestantes();
            int[] coordonneesJouees = new int[2];

            // DEBUG :Console.WriteLine("Abs" + reponseEnvoyee[2]);
            coordonneesJouees[0] = Convert.ToInt32(new string(reponseEnvoyee[2], 1));
            // DEBUG : Console.WriteLine("Abs" + reponseEnvoyee[3]);
            coordonneesJouees[1] = Convert.ToInt32(new string(reponseEnvoyee[3], 1));

            return coordonneesJouees;
        }

        /// <summary>
        /// Evalue si le coup de l'IA est valide. La réponse est donnée par le serveur de jeu.
        /// </summary>
        /// <returns>True si le coup est valide</returns>
        private bool isCoupValide()
        {
            byte[] valideOrNot = null;
            valideOrNot = new byte[2 * sizeof(char)];

            socket.Receive(valideOrNot);
            String answer = Encoding.ASCII.GetString(valideOrNot);

            // Console.WriteLine(answer);

            if (answer.Contains("VALI"))
                return true;

            return false;
        }

        /// <summary>
        /// La méthode analyse le coup du serveur adverse et met à jour les informations de jeu
        /// </summary>
        private void recevoirCoupAdversaire()
        {
            byte[] coupAdversaire = null;

            coupAdversaire = new byte[2 * sizeof(char)];

            socket.Receive(coupAdversaire);
            String reponseServeur = Encoding.ASCII.GetString(coupAdversaire);

            char[] reponseTraitee = reponseServeur.ToCharArray();

            // Console.WriteLine("L'IA Adverse joue " + reponseServeur);
            Console.WriteLine("-> L'adversaire a choisi de jouer l'unité [{0}, {1}] de la carte !", reponseTraitee[2], reponseTraitee[3]);

            int abscisse = Convert.ToInt32(new string(reponseTraitee[2], 1));
            int ordonnee = Convert.ToInt32(new string(reponseTraitee[3], 1));

            //Console.WriteLine(abscisse);
            //Console.WriteLine(ordonnee);

            m_carte[abscisse, ordonnee].Type = 'S';
            DerniereUniteAdversaire = m_carte[abscisse, ordonnee];
        }

        /// <summary>
        /// Evalue si le message du serveur autorise encore le jeu
        /// </summary>
        /// <returns>True si c'est le cas, false sinon</returns>
        private bool laPartieContinue()
        {
            byte[] encoreOuFini = null;

            encoreOuFini = new byte[2 * sizeof(char)];

            socket.Receive(encoreOuFini);
            String reponse = Encoding.ASCII.GetString(encoreOuFini);


            Console.WriteLine("Est-ce que l'on continue ? " + reponse);

            if (reponse.Contains("ENCO"))
                return true;

            return false;
        }

        #endregion

        #region Jouer une Partie => LA BATAILLE DU CAFE

        #region Jouer un coup

        private Unite jouerUnite()
        {
            if (jouerUniteVoisine() != null)
                return jouerUniteVoisine();
            else
                return jouerUniteNonVoisine();
        }

        #endregion

        public void jouerUnePartie()
        {

            do
            {
                int[] coupJoue = envoyerUniteChoisie();

                if (isCoupValide())
                {
                    Console.WriteLine("-> L'IA a choisi de jouer l'unité [{0}, {1}] de la carte !", coupJoue[0], coupJoue[1]);
                    DerniereUnitePlantee = m_carte[coupJoue[0], coupJoue[1]];
                    DerniereUnitePlantee.Type = 'C';
                } else
                {
                    Console.WriteLine("INVALIDE : " + coupJoue[0] + ", " + coupJoue[1]);
                }

                recevoirCoupAdversaire();

                m_carteDeJeu.afficherCarteFrontieres();

                //System.Threading.Thread.Sleep(3000);

            } while (NbGrainesRestantes > 0 && laPartieContinue());

        }

        #endregion

        #endregion
    }
}