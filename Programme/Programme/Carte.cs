﻿using System;

namespace Programme
{
    public class Carte
    {

        #region Constantes

        public const int nbCol = 10;
        public const int nbLine = 10;

        #endregion

        #region Attributs
        
        private Unite[,] carte = new Unite[10, 10];
        private char[,] repCarteAvecFrontieres = new char[21, 21];
        private Parcelle[] parcelles = new Parcelle[17];

        private int premiereParcelleAttribuee = 0;
        private int derniereParcelleAttribuee;
        private const int defaultParcelleValue = Unite.defaultParcelleValue;
        private const int defaultParcelleValueMerForet = defaultParcelleValue + 1;
        private char[] listNomParcelle = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'R', 'S' };

        #endregion

        #region Propriétés

        public Parcelle[] Parcelles
        {
            get { return parcelles; }
        }

        public Unite[,] CarteJeu
        {
            get { return carte; }
        }

        #endregion

        #region Constructeur

        /// <summary>
        /// Constructeur unique qui initialise la carte
        /// </summary>
        /// <param name="trameTraite">Est la trame fournie par le serveur, que l'on aura découpé convenablement (10x10)</param>
        public Carte(int[,] trameTraite)
        {
            for (int x = 0; x < Carte.nbLine; x++)
                for (int y = 0; y < Carte.nbCol; y++)
                {
                    carte[x, y] = new Unite(trameTraite[x, y]);
                    carte[x, y].Abscisse = x;
                    carte[x, y].Ordonnee = y;

                    if (carte[x, y].isJouable() && carte[x, y].Type == 'a' )
                        carte[x, y].Parcelle = defaultParcelleValue;
                    else if (carte[x, y].Type == 'M' || carte[x, y].Type == 'F')
                        carte[x, y].Parcelle = defaultParcelleValueMerForet;
                }

            derniereParcelleAttribuee = premiereParcelleAttribuee;

            // this.afficherCarte();
            this.delimiterParcelles();
            this.initParcelles();
            this.setFrontieresCarte();

            this.afficherCarteFrontieres();
        }

        #endregion

        #region Methodes

        /// <summary>
        /// Propose une représentation graphique simplifiée de la carte de jeu
        /// </summary>
        public void afficherCarte()
        {
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    Console.Write(carte[x, y].Type);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// La méthode évalue les frontières de chacune des cases de la carte et initialise la représentation de la carte
        /// Avec frontières explicites
        /// </summary>
        public void setFrontieresCarte()
        {
            // On a (2 * nbLigne + 2) lignes dans la représentation de la carte avec les frontières, de même pour le nombre de colonnes
            // Toutes les cases paires représentent une frontière possible, et les impaires une case jouable ou non

            // A l'aide de deux boucles, on va parcourir la carte initiale et initialiser les frontières de la représentation graphique

            for (int x = 0; x < Carte.nbLine; x++)
                for (int y = 0; y < Carte.nbCol; y++)
                {
                    if (carte[x, y].isJouable())
                        repCarteAvecFrontieres[2 * x + 1, 2 * y + 1] = listNomParcelle[carte[x, y].Parcelle];
                    else
                        repCarteAvecFrontieres[2 * x + 1, 2 * y + 1] = carte[x, y].Type;
                    
                    // Console.WriteLine("L'unité " + x + " " + y + " appartient à la parcelle " + carte[x, y].Parcelle);

                    if (carte[x, y].FrontiereNord)
                        repCarteAvecFrontieres[x * 2, y * 2 + 1] = '_';

                    if (carte[x, y].FrontiereEst)
                        repCarteAvecFrontieres[x * 2 + 1, y * 2 + 2] = '|';

                    if (carte[x, y].FrontiereSud)
                        repCarteAvecFrontieres[x * 2 + 2, y * 2 + 1] = '_';

                    if (carte[x, y].FrontiereOuest)
                        repCarteAvecFrontieres[2 * x + 1, 2 * y] = '|';

                }
        }

        /// <summary>
        /// Propose une représentation graphique de la carte où les frontières sont explicitement affichées
        /// </summary>
        public void afficherCarteFrontieres()
        {
            setFrontieresCarte();

            for (int x = 0; x < 21; x++)
            {
                for (int y = 0; y < 21; y++)
                {
                    Console.Write(repCarteAvecFrontieres[x, y]);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Permet de mettre à jour les cases jouées par le client et le serveur
        /// </summary>
        /// <param name="posx">La ligne jouée</param>
        /// <param name="posy">La colonne jouée</param>
        /// <param name="new_type">Qui a joué ? S ou C</param>
        public void setType(int posx, int posy, char type)
        {
            carte[posx, posy].Type = type;
        }

        /* What has been done ?
         * - Attribution d'une parcelle à chaque unité
         * - Initialisation des objets Parcelle
         */

        /// <summary>
        /// La méthode permet de trouver la première case jouable de la carte et de récupérer ses coordonnées
        /// </summary>
        /// <returns>La première case jouable de la carte</returns>
        public Unite getPremiereCaseJouable()
        {
            for (int x = 0; x < nbLine; x++)
                for (int y = 0; y < nbCol; y++)
                    if (carte[x, y].isJouable())
                    {
                        carte[x, y].Parcelle = premiereParcelleAttribuee;
                        /* Console.WriteLine("La premiere unité jouable a pour coordonnées : \n"
                                            + "\t - Numéro de ligne : " + x + "\n"
                                            + "\t - Numéro de colonne : " + y + "\n"
                                            + "\t - Parcelle : " + carte[x, y].Parcelle); */
                        return carte[x, y];
                    }

            throw new ArgumentOutOfRangeException("Aucune case jouable n'a été trouvée dans la carte !");
        }

        /// <summary>
        /// La méthode permet d'évaluer toutes les frontières Nord de la ligne passée en paramètre
        /// </summary>
        /// <param name="p_line">Est la ligne à étudier</param>
        private void detecteFrontieresNord(int p_line)
        {
            try
            {
                for (int _col = 0; _col < nbCol; _col++)
                    if (carte[p_line, _col].isJouable())
                        if (carte[p_line, _col].Parcelle == defaultParcelleValue && !carte[p_line, _col].FrontiereNord && carte[p_line - 1, _col].Parcelle != defaultParcelleValue)
                            carte[p_line, _col].Parcelle = carte[p_line - 1, _col].Parcelle;

            } catch { }
        }

        /// <summary>
        /// La méthode permet d'évaluer toutes les frontières Ouest de la ligne passée en paramètre
        /// </summary>
        /// <param name="p_line">Est la ligne à étudier</param>
        private void detecteFrontieresOuest(int p_line)
        {
            try
            {
                for (int _col = 0; _col < nbCol; _col++)
                    if (carte[p_line, _col].isJouable())
                        if (carte[p_line, _col].Parcelle == defaultParcelleValue && !carte[p_line, _col].FrontiereOuest && carte[p_line, _col - 1].Parcelle != defaultParcelleValue)
                            carte[p_line, _col].Parcelle = carte[p_line, _col - 1].Parcelle;

            }
            catch { }
        }

        /// <summary>
        /// La méthode permet d'évaluer toutes les frontières Est de la ligne passée en paramètre
        /// </summary>
        /// <param name="p_line">Est la ligne à étudier</param>
        private void detecteFrontieresEst(int p_line)
        {
            try
            {
                for (int _col = nbCol - 2; _col >= 0; _col--)
                    if (carte[p_line, _col].isJouable())
                        if (carte[p_line, _col].Parcelle == defaultParcelleValue && !carte[p_line, _col].FrontiereEst && carte[p_line, _col + 1].Parcelle != defaultParcelleValue)
                            carte[p_line, _col].Parcelle = carte[p_line, _col + 1].Parcelle;

            }
            catch { }
        }

        /// <summary>
        /// Si une unité a des frontières à O, N et E alors elle n'appartient à aucune parcelle voisine
        /// On lui attribue d'office un nouveau numéro de parcelle
        /// </summary>
        /// <param name="p_line">Est la ligne de la carte à étudier</param>
        private void detecteTripleFrontiere(int p_line)
        {
            try
            {
                for (int _col = 0; _col < nbCol; _col++)
                    if (carte[p_line, _col].isJouable())
                        if (carte[p_line, _col].FrontiereOuest && carte[p_line, _col].FrontiereNord
                                    && carte[p_line, _col].FrontiereEst && carte[p_line, _col].Parcelle == defaultParcelleValue)
                        {
                            attribuerNouvelleParcelle(p_line, _col);
                        }
            }
            catch { }

        }

        /// <summary>
        /// La méthode permet d'attribuer un nouveau numéro de parcelle à l'Unite qui n'en a pas encore
        /// </summary>
        /// <param name="posx">Ligne de l'Unite</param>
        /// <param name="posy">Colonne de l'Unite</param>
        private void attribuerNouvelleParcelle(int posx, int posy)
        {
            try
            {
                if (carte[posx, posy].isJouable() && carte[posx, posy].Parcelle == defaultParcelleValue )
                {
                    derniereParcelleAttribuee++;
                    carte[posx, posy].Parcelle = derniereParcelleAttribuee;
                    // Debug : Console.WriteLine("J'ai attribué la parcelle : " + derniereParcelleAttribuee);
                }
            } catch { }
            
        }

        /// <summary>
        /// La méthode permet de distinguer les différentes unités de la première ligne de la carte
        /// </summary>
        private void delimiterPremiereLigne()
        {
            for (int _col = getPremiereCaseJouable().Ordonnee + 1; _col < nbCol; _col++)
                try
                {
                    if (!carte[getPremiereCaseJouable().Abscisse, _col].FrontiereOuest)
                        carte[getPremiereCaseJouable().Abscisse, _col].Parcelle = carte[getPremiereCaseJouable().Abscisse, _col - 1].Parcelle;
                    else
                        attribuerNouvelleParcelle(getPremiereCaseJouable().Abscisse, _col);
                }
                catch
                {
                    Console.WriteLine("Erreur d'attribution de la ligne à l'indice " + _col);
                }
                
        }

        /// <summary>
        /// Attribue à chaque unité jouable de la carte un numéro de parcelle
        /// </summary>
        public void delimiterParcelles()
        {
            delimiterPremiereLigne();

            for (int ligneCourante = getPremiereCaseJouable().Abscisse; ligneCourante < nbLine; ligneCourante++)
            {
                detecteFrontieresNord(ligneCourante);
                detecteFrontieresOuest(ligneCourante);
                detecteFrontieresEst(ligneCourante);
                detecteTripleFrontiere(ligneCourante);

                // Si un numéro de parcelle n'a pas encore été attribué à l'Unité, on attribue un nouveau numéro de parcelle
                for (int _col = 0; _col < nbCol; _col++)
                {
                    if (carte[ligneCourante, _col].isJouable() && carte[ligneCourante, _col].Parcelle == defaultParcelleValue)
                    {
                        attribuerNouvelleParcelle(ligneCourante, _col);

                        detecteFrontieresNord(ligneCourante);
                        detecteFrontieresOuest(ligneCourante);
                        detecteFrontieresEst(ligneCourante);
                        detecteTripleFrontiere(ligneCourante);
                    }    
                }
            }
            

        }

        /// <summary>
        /// Chaque unité de la carte se voit attribuer un numéro de parcelle (attribut)
        /// Mais pour gérer tout aussi efficacement la carte (pour l'IA), on vient créer de véritables objets Parcelle
        /// Une Parcelle étant une liste d'Unite la composant
        /// </summary>
        public void initParcelles()
        {
            delimiterParcelles();

            for (int x = getPremiereCaseJouable().Abscisse; x < nbLine; x++)
                for (int y = 0; y < nbCol; y++)

                    try
                    {
                        if (carte[x, y].isJouable() && carte[x, y].Parcelle < defaultParcelleValue)
                        {

                            if (parcelles[carte[x, y].Parcelle] == null)
                            {
                                parcelles[carte[x, y].Parcelle] = new Parcelle(carte[x, y].Parcelle);
                                //Console.WriteLine($"La parcelle " + carte[x, y].Parcelle + " a été initialisée !");
                            }

                            parcelles[carte[x, y].Parcelle].addUnite(carte[x, y]);
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Erreur " + listNomParcelle[carte[x, y].Parcelle] + carte[x, y].Parcelle);
                    }

        }

        /// <summary>
        /// Contrôle que la carte est bien composée de 64 Unite sur 16 Parcelles
        /// </summary>
        /// <returns>True si les conditions sont validées, false sinon</returns>
        public bool isCarteValide()
        {
            int nbUnite = 0;

            if (derniereParcelleAttribuee == 16)
            {
                for (int i = 0; i <= derniereParcelleAttribuee; i++)
                {
                    nbUnite = nbUnite + parcelles[i].Taille;
                    Console.WriteLine("Parcelle " + i + " a pour superficie " + parcelles[i].Taille + " Unite");
                }

                if (nbUnite == 64)
                {
                    // Console.WriteLine("La carte est valide !");
                    return true;
                }
            }
               
            Console.WriteLine("La carte n'est pas valide !");
            return false;

        }

        public Unite GetUnite(int x, int y)
        {
            if (x >= 0 && y >= 0)
                return this.carte[x, y];
            else
                return null;
        }


        #endregion

    }
}