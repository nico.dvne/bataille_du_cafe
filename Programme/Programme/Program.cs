﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;

namespace Programme
{
    public class Programm
    {
        public static void Main(string[] args)
        {

            #region Déclaration des variables 

            Carte carteDeJeu;
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            #endregion

            
            Console.WriteLine("La partie débute ici : ");

            #region Traitement de la socket

            try
            {
                /**
                 *172.16.0.88", 1212); //Reseau ethernet de l'IUT interne
                   "51.91.120.237", 1212) //Reseau externe

                 */
                socket.Connect("localhost", 1213); // Préciser l'IP du réseau de l'IUT
                byte[] donneesReceptionnees = new byte[70 * sizeof(int)];
                socket.Receive(donneesReceptionnees);

                Console.WriteLine("Le serveur est connecté !");

                string trameRecue = Encoding.UTF8.GetString(donneesReceptionnees);

                String[] trameDecoupee = trameRecue.Split('|', ':');

                int[,] carteInt = new int[10, 10];

                for (int ligne = 0; ligne < 10; ligne++)
                    for (int col = 0; col < 10; col++)
                        carteInt[ligne, col] = int.Parse(trameDecoupee[(ligne * 10) + col]);

                carteDeJeu = new Carte(carteInt);

                // System.Threading.Thread.Sleep(10000);

                if (carteDeJeu.isCarteValide())
                    Console.WriteLine("La carte est valide et peut être jouée !");
                else
                {
                    throw new Exception("La carte est invalide !");
                }

                #region Initialisation de l'Intelligence

                Intelligence IA = new Intelligence(carteDeJeu, socket);

                IA.jouerUnePartie();

                #endregion

                Console.WriteLine("La partie est maintenant terminée !");
                Console.ReadKey();

                #region Destruction de la socket

                socket.Shutdown(SocketShutdown.Both);
                socket.Close();

                #endregion

            }
            catch (SocketException e)
            {
                carteDeJeu = null;
                Console.WriteLine(e.Message);
            }

            #endregion

            

        }
    }
}