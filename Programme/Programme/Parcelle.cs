﻿using System;
using System.Collections.Generic;

namespace Programme
{
    public class Parcelle
    {
        #region Attributs

        private int numParcelle;
        private List<Unite> listeUnitesParcelle;
        private int taille;

        #endregion

        #region Propriétés

        public int NumParcelle
        {
            get { return numParcelle; }
        }

        public List<Unite> ListeUnitesParcelle
        {
            get { return listeUnitesParcelle; }
            set { listeUnitesParcelle = value; }
        }

        public int Taille
        {
            get { return taille = listeUnitesParcelle.Count;  }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// Constructeur par défaut initialisant la liste des unités composant la parcelle à null
        /// </summary>
        public Parcelle(int _numParcelle)
        {
            listeUnitesParcelle = new List<Unite>();

            if (_numParcelle < 0 || _numParcelle > 16)
                throw new ArgumentException();

            numParcelle = _numParcelle;

        }

        /// <summary>
        /// Constructeur par défaut initialisant la liste des unités composant la parcelle à partir d'une liste source
        /// </summary>
        /// <param name="listeUnitesSource">Liste d'unité composant une parcelle source</param>
        public Parcelle(List<Unite> listeUnitesSource) {
            ListeUnitesParcelle = listeUnitesSource;
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// Permet d'ajouter une unité à une parcelle
        /// </summary>
        /// <param name="uniteAjoutee">Unité à ajouter à la parcelle</param>
        public void addUnite(Unite uniteAjoutee)
        {
            listeUnitesParcelle.Add(uniteAjoutee);
        }

        /// <summary>
        /// Détermine le nombre d'Unites encore jouables sur une Parcelle
        /// </summary>
        /// <returns>Le nombre d'Unites jouables sur une Parcelle</returns>
        public int nbUnitesJouables()
        {
            int nbUnitesJouables = 0;

            foreach (Unite unite in listeUnitesParcelle)
                if (unite.isJouable())
                    nbUnitesJouables++;

            return nbUnitesJouables;
        }

        /// <summary>
        /// La méthode évalue le nombre d'Unite en possession de l'adversaire sur une Parcelle
        /// </summary>
        /// <returns>Le nombre d'Unite en possession de l'adversaire sur une Parcelle</returns>
        public int nbUnitesAdversaire()
        {
            int nbUnitesAdversaire = 0;

            foreach (Unite unite in listeUnitesParcelle)
                if (unite.Type == 'S')
                    nbUnitesAdversaire++;

            return nbUnitesAdversaire;
        }

        public int nbUnitesIA()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// La méthode évalue si l'adversaire contrôle la Parcelle
        /// </summary>
        /// <returns>Renvoie True si c'est le cas, False sinon</returns>
        public bool isControledByEnemy()
        {
            if (nbUnitesAdversaire() >= (Taille / 2) + 1)
                return true;

            return false;
        }

        /// <summary>
        /// La méthode évalue si l'IA contrôle la Parcelle
        /// </summary>
        /// <returns>Renvoie True si c'est le cas, False sinon</returns>
        public bool isControledByIA()
        {
            if (nbUnitesIA() >= (Taille / 2) + 1)
                return true;

            return false;
        }

        #endregion
    }
}
