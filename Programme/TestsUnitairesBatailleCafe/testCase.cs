﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Programme;
namespace TestsUnitairesBatailleCafe
{
    [TestClass]
    public class testCase
    {
        [TestMethod]
        public void TestCaseMer()
        {
            Unite case1 = new Unite(70);
            Assert.AreEqual('M',case1.getType());
            Assert.IsFalse(case1.getFrontiereEst());
            Assert.IsTrue(case1.getFrontiereSud());
            Assert.IsTrue(case1.getFrontiereOuest());
            Assert.IsFalse(case1.getFrontiereNord());
        }

        [TestMethod]
        public void TestCaseForet()
        {
            Unite case1 = new Unite(38);
            Assert.AreEqual('F', case1.getType());
            Assert.IsFalse(case1.getFrontiereEst());
            Assert.IsTrue(case1.getFrontiereSud());
            Assert.IsTrue(case1.getFrontiereOuest());
            Assert.IsFalse(case1.getFrontiereNord());
        }

        [TestMethod]
        public void TestCaseJouable()
        {
            Unite case1 = new Unite(7);
            Assert.AreEqual('a', case1.getType());
            Assert.IsFalse(case1.getFrontiereEst());
            Assert.IsTrue(case1.getFrontiereSud());
            Assert.IsTrue(case1.getFrontiereOuest());
            Assert.IsTrue(case1.getFrontiereNord());
        }
    }
}
